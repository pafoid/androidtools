package controller
{
	import com.pafoid.sharedObject.SharedObjectManager;
	
	import flash.display.NativeMenu;
	import flash.display.NativeMenuItem;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import mx.events.IndexChangedEvent;
	import mx.managers.PopUpManager;
	
	import Utils.ColorUtils;
	
	import command.CommandConstants;
	import command.CommandExecutor;
	import command.CommandHandler;
	import command.ShellEvent;
	
	import data.SharedObjectKeys;
	
	import model.ApplicationModel;
	
	import popups.AboutPopup;
	import popups.AddNewConfigPopup;
	import popups.DeleteConfigPopup;
	import popups.EditConfigPopup;
	import popups.HowToPopup;
	import popups.PreferencesPopup;
	
	public class ApplicationController extends EventDispatcher
	{
		private var _application:AndroidTools;
		private var _consoleController:ConsoleController;
		private var _commandExecutor:CommandExecutor;
		private var _commandHandler:CommandHandler;
		
		public function ApplicationController(application:AndroidTools){
			_application = application;
			_consoleController = new ConsoleController(_application.singleDeployComponent.console);
			_commandExecutor = new CommandExecutor(_consoleController.log);
			_commandHandler = new CommandHandler(this, _consoleController.log);
			_commandExecutor.commandHandler = _commandHandler;
			
			initMenu();
		}
		
		private function initMenu():void{
			var configMenu:NativeMenuItem;
			var helpMenu:NativeMenuItem;
			
			stage.nativeWindow.menu = new NativeMenu();
			
			configMenu = stage.nativeWindow.menu.addItem(new NativeMenuItem("Config"));
			configMenu.submenu = createConfigMenu();
			
			helpMenu = stage.nativeWindow.menu.addItem(new NativeMenuItem("Help"));
			helpMenu.submenu = createHelpMenu();
		}
		
		private function createConfigMenu():NativeMenu{
			var configMenu:NativeMenu = new NativeMenu();
			
			var detectDeviceMenuItem:NativeMenuItem = configMenu.addItem(new NativeMenuItem("Detect Device"));
			detectDeviceMenuItem.addEventListener(Event.SELECT, selectCommand);
			
			configMenu.addItem(new NativeMenuItem("", true));
			
			var addNewMenuItem:NativeMenuItem = configMenu.addItem(new NativeMenuItem("Add New ..."));
			addNewMenuItem.addEventListener(Event.SELECT, selectCommand);
			
			var editMenuItem:NativeMenuItem = configMenu.addItem(new NativeMenuItem("Edit ..."));
			editMenuItem.addEventListener(Event.SELECT, selectCommand);
			
			var deleteMenuItem:NativeMenuItem = configMenu.addItem(new NativeMenuItem("Delete ..."));
			deleteMenuItem.addEventListener(Event.SELECT, selectCommand);
			
			configMenu.addItem(new NativeMenuItem("", true));
			
			var prefMenuItem:NativeMenuItem = configMenu.addItem(new NativeMenuItem("Preferences ..."));
			prefMenuItem.addEventListener(Event.SELECT, selectCommand);
			
			var exitMenuItem:NativeMenuItem = configMenu.addItem(new NativeMenuItem("Exit"));
			exitMenuItem.addEventListener(Event.SELECT, selectCommand);
			
			return configMenu;
		}
		
		private function createHelpMenu():NativeMenu{
			var helpMenu:NativeMenu = new NativeMenu();
			
			var howToMenuItem:NativeMenuItem = helpMenu.addItem(new NativeMenuItem("How To ..."));
			howToMenuItem.addEventListener(Event.SELECT, selectCommand);
			
			var aboutMenuItem:NativeMenuItem = helpMenu.addItem(new NativeMenuItem("About"));
			aboutMenuItem.addEventListener(Event.SELECT, selectCommand);
			
			return helpMenu;
		}
		
		//Shell
		public function initShell():void{
			_consoleController.log("Initializing shell ...", ColorUtils.BLUE);
			
			_commandExecutor.addEventListener(ShellEvent.SHELL_INITIALIZED, onShellInitialized, false, 0, true);
			_application.status = "Initializing shell ...";
			_commandExecutor.initShell();
		}
		
		public function onShellInitialized(event:ShellEvent):void{
			_consoleController.log("Shell initialized !", ColorUtils.GREEN);
			
			_commandExecutor.removeEventListener(ShellEvent.SHELL_INITIALIZED, onShellInitialized);
			
			if(SharedObjectManager.instance.getValue(SharedObjectKeys.DETECT_DEVICES_ON_LAUNCH))
				ApplicationModel.instance.listConnectedAndroidDevices();
			
			_application.status = "Shell initialized";
		}
		
		//Handlers
		protected function selectCommand(event:Event):void{
			if(NativeMenuItem(event.target).label == "Detect Device"){
				_commandExecutor.executeCommand(CommandConstants.LIST_ANDROID_DEVICES);
			}else if(NativeMenuItem(event.target).label == "Add New ..."){
				var newConfigPanel:AddNewConfigPopup = new AddNewConfigPopup();
				
				PopUpManager.addPopUp(newConfigPanel, _application, true);
				PopUpManager.centerPopUp(newConfigPanel);
			}else if(NativeMenuItem(event.target).label == "Edit ..."){
				var editConfigPopup:EditConfigPopup = PopUpManager.createPopUp(_application, EditConfigPopup, true) as EditConfigPopup;
				PopUpManager.centerPopUp(editConfigPopup);
			}else if(NativeMenuItem(event.target).label == "How To ..."){
				var howToPopup:HowToPopup = PopUpManager.createPopUp(_application, HowToPopup, true) as HowToPopup;
				PopUpManager.centerPopUp(howToPopup);
			}else if(NativeMenuItem(event.target).label == "About"){
				var aboutPopup:AboutPopup = PopUpManager.createPopUp(_application, AboutPopup, true) as AboutPopup;
				PopUpManager.centerPopUp(aboutPopup);
			}else if(NativeMenuItem(event.target).label == "Delete ..."){
				var deletePopup:DeleteConfigPopup = PopUpManager.createPopUp(_application, DeleteConfigPopup, true) as DeleteConfigPopup;
				PopUpManager.centerPopUp(deletePopup);
			}else if(NativeMenuItem(event.target).label == "Preferences ..."){
				var prefPopup:PreferencesPopup = PopUpManager.createPopUp(_application, PreferencesPopup, true) as PreferencesPopup;
				PopUpManager.centerPopUp(prefPopup);
			}else if(NativeMenuItem(event.target).label == "Exit"){
				_application.exit();
			}			
		}
		
		public function onChangeTab(event:IndexChangedEvent):void{
			var consoleText:String = _consoleController.console.htmlText; 
			
			if(_application.vs.selectedIndex == _application.vs.getItemIndex(_application.singleDeployComponent)){//SingleDeploy
				_consoleController.console = _application.singleDeployComponent.console;
				_application.singleDeployComponent.console.htmlText = consoleText;
				_application.localDeployController.update();
			}else if(_application.vs.selectedIndex == _application.vs.getItemIndex(_application.batchGeneratorComponent)){//BatchGenerator
				_application.batchGeneratorController.update();
			}else if(_application.vs.selectedIndex == _application.vs.getItemIndex(_application.screenRecordComponent)){//ScreenRecorder
				_consoleController.console = _application.screenRecordComponent.console;
				_application.screenRecordComponent.console.htmlText = consoleText;
				_application.screenRecordController.update();
			}else if(_application.vs.selectedIndex == _application.vs.getItemIndex(_application.packageManagerComponent)){//PackageManager
				_application.packageManagerController.update();
			}else if(_application.vs.selectedIndex == _application.vs.getItemIndex(_application.deviceInfoComponent)){//DeviceInfo
				_consoleController.console = _application.deviceInfoComponent.console;
				_application.deviceInfoComponent.console.htmlText = consoleText;
				_application.deviceInfoController.update();
			}else if(_application.vs.selectedIndex == _application.vs.getItemIndex(_application.screenResizerComponent)){//ScreenResizer
				_consoleController.console = _application.screenResizerComponent.console;
				_application.screenResizerComponent.console.htmlText = consoleText;
				_application.screenResizerController.update();
			}else if(_application.vs.selectedIndex == _application.vs.getItemIndex(_application.remoteDeployComponent)){//RemoteDeploy
				_consoleController.console = _application.remoteDeployComponent.console;
				_application.remoteDeployComponent.console.htmlText = consoleText;
				_application.remoteDeployController.update();
			}/*else if(_application.vs.selectedIndex == _application.vs.getItemIndex(_application.consoleComponent)){//AndroidConsole
				_application.consoleController.update();
			}*/else if(_application.vs.selectedIndex == _application.vs.getItemIndex(_application.advancedComponent)){//AdvancedTools
				_consoleController.console = _application.advancedComponent.console;
				_application.advancedComponent.console.htmlText = consoleText;
				_application.advancedToolsController.update();
			}
			
			/*if(event.oldIndex == _application.vs.getItemIndex(_application.consoleComponent)){
				_application.consoleController.onStopMonitoring(null);
			}*/
		}
		
		public function setStatusText(value:String):void{
			_application.status = value;
		}
		
		//Getters/Setters
		private function get stage():Stage{
			return _application.stage;
		}

		public function get commandExecutor():CommandExecutor
		{
			return _commandExecutor;
		}

		public function get consoleController():ConsoleController
		{
			return _consoleController;
		}

		public function get commandHandler():CommandHandler
		{
			return _commandHandler;
		}

		public function get application():AndroidTools
		{
			return _application;
		}


	}
}