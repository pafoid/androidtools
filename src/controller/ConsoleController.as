package controller
{
	import flash.utils.setTimeout;
	
	import mx.controls.RichTextEditor;
	import mx.events.FlexEvent;
	
	import Utils.ColorUtils;

	public class ConsoleController
	{
		private var _console:RichTextEditor;
		private var _isLogging:Boolean;
		
		private var _repeatFirstMessage:Boolean;
		private var _firstMessage:String;
		
		public function ConsoleController(consoleComponent:RichTextEditor, repeatFirstMessage:Boolean = false)
		{
			_console = consoleComponent;
			_repeatFirstMessage = repeatFirstMessage;
		}
		
		//Public functions
		public function log(msg:String, color:String = ColorUtils.YELLOW):void{
			if(_isLogging){
				setTimeout(log, 500, msg, color);
				return;
			}
			
			if(!_repeatFirstMessage && msg != "Initializing shell ..." && msg != "Shell initialized !" && msg != ""){
				if(_firstMessage == null){
					_firstMessage = msg;
					trace("_firstMessage : "+_firstMessage);
					return;
				}else if(msg == _firstMessage && msg != null && msg != ""){
					trace("Not repeating first message");
					return;
				}
			}
			
			_isLogging = true;
			if(msg == ""){
				trace("mesage is null");
				_isLogging = false;
				return;
			}
			
			var htmlText:String = msg;
			htmlText = htmlText.split("\r\n\r\n").join("\n");
			htmlText = htmlText.split("\r\r\n").join("\n");
			htmlText = htmlText.split("\r\n\t").join("\n");
			//htmlText = htmlText.split("\r\n").join("\n");
			htmlText = htmlText.split("\r\n").join("");
			
			htmlText = htmlText.split("<").join("&lt;");
			htmlText = htmlText.split(">").join("&gt;");
			
			htmlText = "<b><font color='"+color+"'>"+htmlText+"</font></b>";
			
			if(msg.substr(msg.length-2, 2)!="\n"){
				htmlText += "\n";
			}
			
			htmlText = htmlText.split("\r\n</font>\n").join("</font>\n");
			
			if(_console){
				_console.addEventListener(FlexEvent.VALUE_COMMIT, onConsoleValueCommited, false, 0, true);
				_console.htmlText += htmlText;
			}
		}
		
		public function clearConsole():void{
			_console.htmlText = "";
		}
		
		//Handlers
		private function onConsoleValueCommited(event:FlexEvent):void{
			trace("onConsoleValueCommited");
			
			_isLogging = false;
			if(_console){
				_console.textArea.verticalScrollPosition=_console.textArea.maxVerticalScrollPosition;
				_console.removeEventListener(FlexEvent.VALUE_COMMIT, onConsoleValueCommited);
			}
		}
		
		
		//Getters/Setters
		public function get console():RichTextEditor{
			return _console;
		}

		public function set console(value:RichTextEditor):void{
			_console = value;
		}

		public function get firstMessage():String
		{
			return _firstMessage;
		}

	}
}