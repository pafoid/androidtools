package view.controller
{
	import flash.events.MouseEvent;
	
	import spark.events.IndexChangeEvent;
	
	import controller.ApplicationController;

	public class ToolbarController
	{
		private var _applicationController:ApplicationController;
		
		public function ToolbarController(applicationController:ApplicationController)
		{
			_applicationController = applicationController;
		}
		
		//Handlers
		public function onClickDetectDevices(event:MouseEvent):void{
			
		}
		
		public function onClickClearConsole(event:MouseEvent):void{
			
		}
		
		public function onClickExportConsole(event:MouseEvent):void{
			
		}
		
		public function onClickPreferences(event:MouseEvent):void{
			
		}
		
		public function onClickAbout(event:MouseEvent):void{
			
		}
		
		public function onClickHelp(event:MouseEvent):void{
			
		}
		
		public function onClickAddConfig(event:MouseEvent):void{
			
		}
		
		public function onClickEditConfig(event:MouseEvent):void{
			
		}
		
		public function onSelectDevice(event:IndexChangeEvent):void{
			
		}
	}
}