package view.controller.impl
{
	import command.CommandConstants;
	
	import controller.ApplicationController;
	
	import flash.desktop.NativeProcess;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.filesystem.File;
	
	import view.controller.BaseTabController;

	public class AndroidConsoleController extends BaseTabController
	{
		
		public function AndroidConsoleController(applicationController:ApplicationController)
		{
			super(applicationController);
		}
		
		//Public functions 
		override public function update():void{
			onStartMonitoring(null);
		}
		
		//Handlers
		public function onStartMonitoring(event:MouseEvent):void{
			_applicationController.commandExecutor.stopListeningForOutput();
			_applicationController.commandExecutor.process.addEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, onOutputData, false, 1000);
			
			_applicationController.commandExecutor.executeCommand(CommandConstants.LOG_CAT);
		}
		
		public function onStopMonitoring(event:MouseEvent):void{
			_applicationController.commandExecutor.executeCommand("exit"+File.lineEnding);
			_applicationController.commandExecutor.startListeningForOutput();
		}
		
		override public function onClearConsole(event:MouseEvent):void{
			//_applicationController.application.consoleComponent.console.htmlText = "";
		}
		
		private function onOutputData(event:ProgressEvent):void{
			var msg:String = NativeProcess(event.target).standardOutput.readUTFBytes(_applicationController.commandExecutor.process.standardOutput.bytesAvailable);
			trace("Got: ", msg);
			
			//_applicationController.application.consoleComponent.console.htmlText += msg;
		}
	}
}