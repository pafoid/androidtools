package view.controller.impl
{
	import com.pafoid.string.StringUtils;
	
	import flash.events.MouseEvent;
	
	import command.CommandConstants;
	
	import controller.ApplicationController;
	
	import view.controller.BaseTabController;
	
	public class ScreenResizerController extends BaseTabController
	{
		public function ScreenResizerController(applicationController:ApplicationController)
		{
			super(applicationController);
		}
		
		//Public functions
		override public function update():void{
			if(_commandExecutor.shellInitialized && !_commandHandler.hasRoot)
				_commandHandler.checkRootAccess();
		}
		
		//Private functions
		
		
		//Handlers
		public function onClickResize(event:MouseEvent):void{
			var cmd:String = (_commandHandler.hasRoot) ? CommandConstants.SCREEN_ROOT_SET_SIZE : CommandConstants.SCREEN_SET_SIZE;
			var screenWidth:String = _application.screenResizerComponent.widthInput.text;
			var screenHeight:String = _application.screenResizerComponent.heightInput.text;
			
			_commandExecutor.executeCommand(StringUtils.replaceNumberedTokens(cmd, [screenWidth, screenHeight]));
		}
		
		public function onClickReset(event:MouseEvent):void{
			var cmd:String = /*(_commandHandler.hasRoot) ?*/ CommandConstants.SCREEN_RESET_SIZE;
			_commandExecutor.executeCommand(cmd);
		}
	}
}