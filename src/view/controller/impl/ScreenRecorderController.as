package view.controller.impl
{
	import Utils.ColorUtils;
	
	import com.pafoid.mobile.data.DeviceVO;
	import com.pafoid.sharedObject.SharedObjectManager;
	import com.pafoid.string.StringUtils;
	
	import command.CommandConstants;
	import command.CommandExecutor;
	
	import controller.ApplicationController;
	import controller.ConsoleController;
	
	import data.SharedObjectKeys;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	import flash.utils.clearTimeout;
	import flash.utils.setTimeout;
	
	import model.ApplicationModel;
	
	import mx.controls.Alert;
	
	import spark.events.IndexChangeEvent;
	
	import view.controller.BaseTabController;

	public class ScreenRecorderController extends BaseTabController
	{
		private var _verbose:Boolean;
		private var _rotate:Boolean;
		private var _saveOnDesktop:Boolean;
		private var _deleteFromDevice:Boolean;
		private var _width:Number;
		private var _height:Number;
		private var _timeLimit:int;
		private var _bitRate:int;
		private var _fileName:String;
		
		private var _saveOnDesktopTimeOut:uint;
		private var _deleteFromDeviceTimeOut:uint;
		
		public function ScreenRecorderController(applicationController:ApplicationController)
		{
			super(applicationController);			
		}
		
		//Public methods
		override public function update():void{
			if(_application.screenRecordComponent.binaryInput4 && SharedObjectManager.instance.getStringValue(SharedObjectKeys.LAST_VIDEO_FILE) != null){
				_application.screenRecordComponent.binaryInput4.text = SharedObjectManager.instance.getStringValue(SharedObjectKeys.LAST_VIDEO_FILE);
				_application.screenRecordComponent.timeInput.text = SharedObjectManager.instance.getStringValue(SharedObjectKeys.LAST_VIDEO_TIME_LIMIT);
				_application.screenRecordComponent.bitrateInput.text = SharedObjectManager.instance.getStringValue(SharedObjectKeys.LAST_VIDEO_BIT_RATE);
				_application.screenRecordComponent.widthInput.text = SharedObjectManager.instance.getStringValue(SharedObjectKeys.LAST_VIDEO_WIDTH);
				_application.screenRecordComponent.heightInput.text = SharedObjectManager.instance.getStringValue(SharedObjectKeys.LAST_VIDEO_HEIGHT);
				
				_application.screenRecordComponent.verboseChkBox.selected = SharedObjectManager.instance.getValue(SharedObjectKeys.LAST_VIDEO_VERBOSE);
				_application.screenRecordComponent.rotateChkBox.selected = SharedObjectManager.instance.getValue(SharedObjectKeys.LAST_VIDEO_ROTATE);
				_application.screenRecordComponent.saveOnDesktopChkBox.selected = SharedObjectManager.instance.getValue(SharedObjectKeys.LAST_VIDEO_ON_DESKTOP);
				_application.screenRecordComponent.deleteFromDeviceChkBox.selected = SharedObjectManager.instance.getValue(SharedObjectKeys.DELETE_FILE_FLAG);
			}
			
			if(ApplicationModel.instance.androidDeviceList && _application.screenRecordComponent.deviceComboBox){
				_application.screenRecordComponent.deviceComboBox.dataProvider = ApplicationModel.instance.androidDeviceList;
				_application.screenRecordComponent.deviceComboBox.selectedIndex = 0;
				
				_application.screenRecordComponent.recordButton.enabled = (_application.screenRecordComponent.deviceComboBox.dataProvider.length > 0);
			}
		}
		
		//Handlers
		public function onCheckVerbose(event:Event):void{
			_verbose = _application.screenRecordComponent.verboseChkBox.selected;
		}
		
		public function onCheckRotate(event:Event):void{
			_rotate = _application.screenRecordComponent.rotateChkBox.selected;
		}
		
		public function onCheckSaveOnDesktop(event:Event):void{
			_saveOnDesktop = _application.screenRecordComponent.saveOnDesktopChkBox.selected;
		}
		
		public function onSelectDevice(event:IndexChangeEvent):void{
			super.onDeviceSelected(_application.screenRecordComponent.deviceComboBox.selectedItem);
		}
		
		public function onCheckDeleteFromDevice(event:Event):void{
			_deleteFromDevice = _application.screenRecordComponent.deleteFromDeviceChkBox.selected;
		}
		
		public function startRecording(event:Event):void{
			_width = parseInt(_application.screenRecordComponent.widthInput.text);
			_height = parseInt(_application.screenRecordComponent.heightInput.text);
			_bitRate = parseInt(_application.screenRecordComponent.bitrateInput.text) * 100000;
			_timeLimit = parseInt(_application.screenRecordComponent.timeInput.text);	
			_fileName = _application.screenRecordComponent.binaryInput4.text;
			_fileName = (_fileName.indexOf(".mp4")<0) ? _fileName +".mp4" : _fileName;
			
			if(validate()){
				saveConfig();
				
				var serial:String = DeviceVO(_application.screenRecordComponent.deviceComboBox.selectedItem).serial;
				var rotate:String = (_rotate) ? " --rotate" : "";
				var verbose:String = (_verbose) ? " --verbose" : "";
				
				var command:String = StringUtils.replaceNumberedTokens(CommandConstants.START_SCREEN_RECORD, [serial, _width, _height, _bitRate, _timeLimit, rotate, verbose, _fileName]);
				_commandExecutor.executeCommand(command);
				
				if(_saveOnDesktop){
					_saveOnDesktopTimeOut = setTimeout(copyToDesktop, (_timeLimit+1)*1000);
				}else if(_deleteFromDevice){
					_deleteFromDeviceTimeOut = setTimeout(deleteFromDevice, (_timeLimit+1)*1000);
				}
			}			
		}
		
		public function stopRecording(event:Event):void{
			//_commandExecutor.abortCommand();
		}
		
		public function showHelp(event:Event):void{
			_commandExecutor.executeCommand(CommandConstants.SHOW_SCREEN_RECORD_HELP);
		}
		
		private function saveConfig():void{
			SharedObjectManager.instance.save(SharedObjectKeys.LAST_VIDEO_FILE, _fileName);
			SharedObjectManager.instance.save(SharedObjectKeys.LAST_VIDEO_TIME_LIMIT, _timeLimit);
			SharedObjectManager.instance.save(SharedObjectKeys.LAST_VIDEO_BIT_RATE, _bitRate/100000);
			SharedObjectManager.instance.save(SharedObjectKeys.LAST_VIDEO_WIDTH, _width);
			SharedObjectManager.instance.save(SharedObjectKeys.LAST_VIDEO_HEIGHT, _height);
			SharedObjectManager.instance.save(SharedObjectKeys.LAST_VIDEO_VERBOSE, _verbose);
			SharedObjectManager.instance.save(SharedObjectKeys.LAST_VIDEO_ROTATE, _rotate);
			SharedObjectManager.instance.save(SharedObjectKeys.LAST_VIDEO_ON_DESKTOP, _saveOnDesktop);
			SharedObjectManager.instance.save(SharedObjectKeys.DELETE_FILE_FLAG, _deleteFromDevice);
		}
		
		private function validate():Boolean{
			if(_bitRate < 100000 || _bitRate > 100000000){
				Alert.show("Bitrate needs to be higher than 1 and lower than 10.", "Error");
				_consoleController.log("Bitrate needs to be higher than 0.", ColorUtils.RED);
				return false;
			}
			
			if(_width<1){
				Alert.show("Video width needs to be higher than 0.", "Error");
				_consoleController.log("Video width needs to be higher than 0.", ColorUtils.RED);
				return false;
			}
			
			if(_height<1){
				Alert.show("Video height needs to be higher than 0.", "Error");
				_consoleController.log("Video height needs to be higher than 0.", ColorUtils.RED);
				return false;
			}
			
			if(_timeLimit<1){
				Alert.show("Video length needs to be higher than 0.", "Error");
				_consoleController.log("Video length needs to be higher than 0.", ColorUtils.RED);
				return false;
			}
						
			return true;
		}
		
		private function copyToDesktop():void{
			trace("copyToDesktop");
			clearTimeout(_saveOnDesktopTimeOut);
			
			var cmd:String = StringUtils.replaceNumberedTokens(CommandConstants.PULL_FILE, [ApplicationModel.instance.currentDeviceVO.serial, "/sdcard/"+_fileName, File.desktopDirectory.nativePath+"/"+_fileName]);
			_commandExecutor.executeCommand(cmd);
			
			if(_deleteFromDevice)
				deleteFromDevice();
		}
		
		private function deleteFromDevice():void{
			trace("deleteFromDevice");			
			clearTimeout(_deleteFromDeviceTimeOut);
			
			var cmd:String = StringUtils.replaceNumberedTokens(CommandConstants.DELETE_FILE, [ApplicationModel.instance.currentDeviceVO.serial, "/sdcard/"+_fileName]);
			_commandExecutor.executeCommand(cmd);
		}
	}
}