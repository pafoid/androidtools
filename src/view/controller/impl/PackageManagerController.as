package view.controller.impl
{
	import com.pafoid.mobile.data.DeviceVO;
	import com.pafoid.mobile.data.PackageVO;
	import com.pafoid.sharedObject.SharedObjectManager;
	import com.pafoid.string.StringUtils;
	
	import command.CommandConstants;
	
	import controller.ApplicationController;
	
	import data.SharedObjectKeys;
	
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	
	import model.ApplicationModel;
	
	import spark.events.IndexChangeEvent;
	
	import view.controller.BaseTabController;

	public class PackageManagerController extends BaseTabController
	{

		public function PackageManagerController(applicationController:ApplicationController)
		{
			super(applicationController);
		}
		
		//Public methods
		override public function update():void{
			if(_applicationController.application.packageManagerComponent.deviceComboBox){
				_applicationController.application.packageManagerComponent.deviceComboBox.dataProvider = ApplicationModel.instance.androidDeviceList;
				_applicationController.application.packageManagerComponent.deviceComboBox.selectedIndex = 0;
				
				_applicationController.application.packageManagerComponent.detectPackagesButton.enabled = (_applicationController.application.packageManagerComponent.deviceComboBox.dataProvider.length > 0);
			}
		}
		
		//Handlers
		public function onSelectDevice(event:IndexChangeEvent):void{
			super.onDeviceSelected(_applicationController.application.packageManagerComponent.deviceComboBox.selectedItem);
		}
		
		public function onClickDetectPackages(event:MouseEvent):void
		{
			_applicationController.commandExecutor.executeCommand(CommandConstants.LIST_PACKAGES);
		}
		
		public function onClickUninstallPackage(event:MouseEvent):void
		{
			var selectedPackageName:String = _applicationController.application.packageManagerComponent.packageList.selectedItem.name;
			_applicationController.commandExecutor.executeCommand(StringUtils.replaceNumberedTokens(CommandConstants.UNINSTALL_PACKAGE, [selectedPackageName]));
		}
		
		public function onSelectPackage(event:IndexChangeEvent):void
		{
			_applicationController.application.packageManagerComponent.uninstallPackageButton.enabled = (event.newIndex != -1);
			_applicationController.application.packageManagerComponent.backupPackageButton.enabled = (event.newIndex != -1);
		}
		
		public function onClickBackupPackage(event:MouseEvent):void{
			var location:String = PackageVO(_applicationController.application.packageManagerComponent.packageList.selectedItem).location;
			var fileName:String = PackageVO(_applicationController.application.packageManagerComponent.packageList.selectedItem).fileName;
			
			var fileLocation:String = SharedObjectManager.instance.getStringValue(SharedObjectKeys.PACKAGE_BACKUP_LOCATION)+"/"+fileName;
			var cmd:String = StringUtils.replaceNumberedTokens(CommandConstants.PULL_FILE, [ApplicationModel.instance.currentDeviceVO.serial, location, fileLocation]);
			_applicationController.commandExecutor.executeCommand(cmd);
		}
	}
}