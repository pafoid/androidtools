package view.controller.impl
{
	import com.pafoid.mobile.data.DeviceVO;
	import com.pafoid.sharedObject.SharedObjectManager;
	import com.pafoid.string.StringUtils;
	
	import flash.events.Event;
	import flash.events.FileListEvent;
	import flash.events.FocusEvent;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.FileFilter;
	
	import mx.collections.ArrayList;
	import mx.controls.Alert;
	import mx.managers.PopUpManager;
	
	import spark.events.IndexChangeEvent;
	
	import Utils.ColorUtils;
	
	import command.CommandConstants;
	
	import controller.ApplicationController;
	
	import data.SharedObjectKeys;
	
	import model.ApplicationModel;
	
	import popups.CustomCommandPopup;
	
	import view.controller.BaseTabController;

	public class LocalDeployController extends BaseTabController
	{
		private var _binaryFocused:Boolean = false;
		private var _binary:File;
		
		private var _binaries:File;
		private var _binaryArray:Array = [];

		public function LocalDeployController(applicationController:ApplicationController)
		{
			super(applicationController);
		}
		
		//Public methods
		override public function update():void{
			var lastBinaryLocation:String = SharedObjectManager.instance.getStringValue(SharedObjectKeys.LAST_DEPLOYED_BINARY);
			if(lastBinaryLocation){
				_application.singleDeployComponent.binaryInput.text = lastBinaryLocation;
				_binary = new File().resolvePath(lastBinaryLocation);
			}
			
			if(ApplicationModel.instance.androidDeviceList){
				_application.singleDeployComponent.deviceComboBox.dataProvider = ApplicationModel.instance.androidDeviceList;
				_application.singleDeployComponent.deviceComboBox.selectedIndex = 0;
			}
			
			_application.singleDeployComponent.reinstallChkBox.selected = SharedObjectManager.instance.getValue(SharedObjectKeys.REINSTALL_FLAG);
			_application.singleDeployComponent.deployButton.enabled = (_application.singleDeployComponent.deviceComboBox.dataProvider.length > 0);
		}
		
		//Private methods
		private function validate():Boolean
		{
			if(!_binary){
				Alert.show("You must select a apk or ipa file.", "Error");
				_consoleController.log("You must select a apk or ipa file.", ColorUtils.RED);
				return false;
			}
			
			if(_application.singleDeployComponent.deviceComboBox.selectedIndex == -1){
				Alert.show("You must select a device to deploy "+_binary.name+" to.", "Error");
				_consoleController.log("You must select a device to deploy "+_binary.name+" to.", ColorUtils.RED);
				return false;					
			}
			
			if(_application.singleDeployComponent.connectionComboBox.selectedIndex == -1){
				Alert.show("You must select a connection type.", "Error");
				_consoleController.log("You must select a connection type.", ColorUtils.RED);
				return false;
			}
			
			if(_binary.extension == "apk" && DeviceVO(_application.singleDeployComponent.deviceComboBox.selectedItem).type != DeviceVO.ANDROID_DEVICE /*or emu*/){
				Alert.show("You selected an apk file which needs to be installed on an Android device.", "Error");
				_consoleController.log("You selected an apk file which needs to be installed on an Android device.", ColorUtils.RED);
				return false;
			}
			
			if(_binary.extension == "ipa" && DeviceVO(_application.singleDeployComponent.deviceComboBox.selectedItem).type != DeviceVO.IOS){
				Alert.show("You selected an ipa file which needs to be installed on an Apple device.", "Error");
				_consoleController.log("You selected an ipa file which needs to be installed on an Apple device.", ColorUtils.RED);
				return false;
			}
			
			return true;
		}
		
		private function executeDeploy():void{
			trace("executeDeploy");
			
			//Install
			//iOS
			if(DeviceVO(_application.singleDeployComponent.deviceComboBox.selectedItem).type == DeviceVO.IOS){
				_commandExecutor.executeCommand(StringUtils.replaceNumberedTokens(CommandConstants.INSTALL_IPA, [DeviceVO(_application.singleDeployComponent.deviceComboBox.selectedItem).serial, _binary.nativePath]));
			}else{//Android
				var reinstallFlag:String = (_application.singleDeployComponent.reinstallChkBox.selected) ? "-r" : "";
				
				if(_application.singleDeployComponent.connectionComboBox.selectedIndex == 1){//WIFI
					_commandExecutor.executeCommand(CommandConstants.WAIT_FOR_DEVICE);
					_commandExecutor.executeCommand(StringUtils.replaceNumberedTokens(CommandConstants.CONNECT_TO_WIFI, [DeviceVO(_application.singleDeployComponent.deviceComboBox.selectedItem).ip]));
					_commandExecutor.executeCommand(StringUtils.replaceNumberedTokens(CommandConstants.INSTALL_APK, [reinstallFlag, _binary.nativePath]));
				}else{//USB
					if(ApplicationModel.instance.androidDeviceList.length > 1)
						_commandExecutor.executeCommand(StringUtils.replaceNumberedTokens(CommandConstants.INSTALL_APK_BY_SERIAL, [DeviceVO(_application.singleDeployComponent.deviceComboBox.selectedItem).serial, reinstallFlag, _binary.nativePath]));
					else
						_commandExecutor.executeCommand(StringUtils.replaceNumberedTokens(CommandConstants.INSTALL_APK, [reinstallFlag, _binary.nativePath]));
				}
			}
			
			SharedObjectManager.instance.save(SharedObjectKeys.REINSTALL_FLAG, _application.singleDeployComponent.reinstallChkBox.selected);
		}
		
		/*private function executeMultipleDeploy():void{
		trace("executeMultipleDeploy");
		
		for each (var file:File in _binaryArray){
		if(file.extension == "apk"){
		for each (var androidDevice:DeviceVO in _application.androidDeviceList.selectedItems){
		_commandExecutor.executeCommand('adb -s '+androidDevice.serial+' install -r "'+file.nativePath+'"'+File.lineEnding);							
		}
		}else if(file.extension == "ipa"){
		for each (var iosDevice:DeviceVO in _application.iosDeviceList.selectedItems){
		_commandExecutor.executeCommand('adt -installApp -platform ios -device '+iosDevice.serial+' -package "'+file.nativePath+'"'+File.lineEnding);							
		}
		}
		}				
		}*/
		
		private function loadDevices():void{
			//Load configs
			var configFile:File = File.applicationStorageDirectory.resolvePath("devices.json");
			var fileStream:FileStream = new FileStream(); 
			fileStream.open(configFile, FileMode.READ);
			var fileContent:String = fileStream.readUTFBytes(fileStream.bytesAvailable);
			var configs:Object = (fileContent != "") ? JSON.parse(fileContent) : new Object();
			var configsArray:Array = [];
			
			for each (var device:Object in configs){
				var vo:DeviceVO = new DeviceVO(device.id, device.ip, device.type, device.serial);
				configsArray.push(vo);
			}
			
			_application.singleDeployComponent.deviceComboBox.dataProvider = new ArrayList(configsArray);
			_consoleController.log("Devices Loaded", ColorUtils.GREEN);
		}
		
		//Handlers
		public function onBinaryFocus(event:FocusEvent):void{
			if(!_binaryFocused){
				_binary = new File();
				_binary.addEventListener(Event.SELECT, onSelectBinary);
				_binary.addEventListener(Event.CANCEL, onCancelBinary);
				_binary.browse([new FileFilter("Android/iOS Application Package", "*.apk;*.ipa")]);
				
				_binaryFocused = true;
			}
		}
		
		public function onSelectBinary(event:Event):void{
			_binaryFocused = false;
			_application.singleDeployComponent.binaryInput.text = event.target.nativePath;//_binary.name;
			_application.vs.setFocus();
			_consoleController.log("Selected binary : "+_binary.name, ColorUtils.BLUE);
		}
		
		public function onCancelBinary(event:Event):void{
			trace("cancelHandler: " + event);
			_binaryFocused = false;
			_application.vs.setFocus();
		}
		
		public function onBinariesFocus(event:FocusEvent):void{
			if(!_binaryFocused){
				_binaries = new File();
				_binaries.addEventListener(FileListEvent.SELECT_MULTIPLE, onSelectBinaries);
				_binaries.addEventListener(Event.CANCEL, onCancelBinary);
				_binaries.browseForOpenMultiple("Select binaries", [new FileFilter("Android/iOS Application Package", "*.apk;*.ipa")]);
				_binaryFocused = true;
			}
		}
		
		public function onSelectBinaries(event:FileListEvent):void{
			_binaryFocused = false;
			_consoleController.log("Selected binaries : \n", ColorUtils.BLUE);
			
			for each (var file:File in event.files){
				_consoleController.log(file.name+"\n");
				if(_binaryArray.indexOf(file) == -1)
					_binaryArray.push(file);
			}				
			
			_application.vs.setFocus();				
		}
		
		public function onDeploy(event:MouseEvent):void{
			trace("deploy");
			_consoleController.log("Validating deploy options ...", ColorUtils.BLUE);
			if(!validate())
				return;
			
			_consoleController.log("Deploying package ...", ColorUtils.BLUE);
			SharedObjectManager.instance.save(SharedObjectKeys.LAST_DEPLOYED_BINARY, _application.singleDeployComponent.binaryInput.text);
			
			executeDeploy();
		}
		
		public function onDeployMultiple(event:MouseEvent):void{
			trace("deploy multiple");
			_consoleController.log("Validating deploy options ...", ColorUtils.BLUE);
			
			_consoleController.log("Deploying package ...", ColorUtils.BLUE);
			
			//executeMultipleDeploy();
		}
		
		public function onSelectConnection(event:IndexChangeEvent):void{
			trace("Selected connection type : "+_application.singleDeployComponent.connectionComboBox.selectedItem);
			
			if(_application.singleDeployComponent.connectionComboBox.selectedItem == "USB"){
				if(_application.singleDeployComponent.binaryInput.text.indexOf("ipa")>-1){
					ApplicationModel.instance.listConnectediOSDevices();	
				}else if(_application.singleDeployComponent.binaryInput.text.indexOf("apk")>-1){
					ApplicationModel.instance.listConnectedAndroidDevices();	
				}
			}else{
				loadDevices();
			}
		}
		
		public function onSelectConnection2(event:IndexChangeEvent):void{
			if(_application.singleDeployComponent.connectionComboBox.selectedItem == "USB"){
				ApplicationModel.instance.listConnectediOSDevices();	
				ApplicationModel.instance.listConnectedAndroidDevices();
			}
		}
		
		public function onSelectDevice(event:IndexChangeEvent):void{
			super.onDeviceSelected(_application.singleDeployComponent.deviceComboBox.selectedItem);
		}
		
		public function onHelpADB(event:MouseEvent):void{
			_commandExecutor.executeCommand(CommandConstants.ADB_HELP);	
		}
		
		public function onHelpADT(event:MouseEvent):void{
			_commandExecutor.executeCommand(CommandConstants.ADT_HELP);
		}
		
		public function onCustomCommandClick(event:MouseEvent):void{
			var customCommandPopup:CustomCommandPopup = new CustomCommandPopup();
			customCommandPopup.commandFunction = _commandExecutor.executeCommand;
			PopUpManager.addPopUp(customCommandPopup, _application, false);
			PopUpManager.centerPopUp(customCommandPopup);
		}
		
		public function onResetConfig(event:MouseEvent):void{
			_application.singleDeployComponent.deviceComboBox.selectedIndex = -1;
			_application.singleDeployComponent.binaryInput.text = "";
			_application.singleDeployComponent.connectionComboBox.selectedIndex = -1;
		}
	}
}