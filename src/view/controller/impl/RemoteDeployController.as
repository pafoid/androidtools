package view.controller.impl
{
	import Utils.ColorUtils;
	
	import com.pafoid.mobile.data.DeviceVO;
	import com.pafoid.sharedObject.SharedObjectManager;
	import com.pafoid.string.StringUtils;
	import com.pafoid.validation.URLValidation;
	
	import command.CommandConstants;
	import command.CommandExecutor;
	import command.CommandHandler;
	
	import controller.ApplicationController;
	import controller.ConsoleController;
	
	import data.SharedObjectKeys;
	
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.FileReference;
	import flash.net.URLRequest;
	
	import model.ApplicationModel;
	
	import mx.collections.ArrayList;
	import mx.controls.Alert;
	import mx.managers.PopUpManager;
	
	import popups.CustomCommandPopup;
	
	import spark.events.IndexChangeEvent;
	
	import view.controller.BaseTabController;

	public class RemoteDeployController extends BaseTabController
	{
		private var _binary:File;
		
		private var _binaries:File;
		private var _binaryArray:Array = [];
		
		private var _binaryFileReference:FileReference;
		
		public function RemoteDeployController(applicationController:ApplicationController)
		{
			super(applicationController);
		}
		
		//Public methods
		override public function update():void{
			var lastBinaryLocation:String = SharedObjectManager.instance.getStringValue(SharedObjectKeys.LAST_DEPLOYED_REMOTE_BINARY);
			if(lastBinaryLocation && _application.remoteDeployComponent.binaryInput){
				_application.remoteDeployComponent.binaryInput.text = lastBinaryLocation;
				_binary = new File().resolvePath(lastBinaryLocation);
			}
			
			if(ApplicationModel.instance.androidDeviceList && _application.remoteDeployComponent.deviceComboBox){
				_application.remoteDeployComponent.deviceComboBox.dataProvider = ApplicationModel.instance.androidDeviceList;
				_application.remoteDeployComponent.deviceComboBox.selectedIndex = 0;
			}
			
			if(_application.remoteDeployComponent.reinstallChkBox){
				_application.remoteDeployComponent.reinstallChkBox.selected = SharedObjectManager.instance.getValue(SharedObjectKeys.REMOTE_REINSTALL_FLAG);
				_application.remoteDeployComponent.deployButton.enabled = (_application.remoteDeployComponent.deviceComboBox.dataProvider.length > 0);
			}
		}
		
		//Private methods
		private function validate():Boolean
		{
			if(!_binary){
				Alert.show("You must select a apk or ipa file.", "Error");
				_consoleController.log("You must select a apk or ipa file.", ColorUtils.RED);
				return false;
			}
			
			if(_application.remoteDeployComponent.deviceComboBox.selectedIndex == -1){
				Alert.show("You must select a device to deploy "+_binary.name+" to.", "Error");
				_consoleController.log("You must select a device to deploy "+_binary.name+" to.", ColorUtils.RED);
				return false;					
			}
			
			if(_application.remoteDeployComponent.connectionComboBox.selectedIndex == -1){
				Alert.show("You must select a connection type.", "Error");
				_consoleController.log("You must select a connection type.", ColorUtils.RED);
				return false;
			}
			
			if(_binary.extension == "apk" && DeviceVO(_application.remoteDeployComponent.deviceComboBox.selectedItem).type != DeviceVO.ANDROID_DEVICE /*or emu*/){
				Alert.show("You selected an apk file which needs to be installed on an Android device.", "Error");
				_consoleController.log("You selected an apk file which needs to be installed on an Android device.", ColorUtils.RED);
				return false;
			}
			
			if(_binary.extension == "ipa" && DeviceVO(_application.remoteDeployComponent.deviceComboBox.selectedItem).type != DeviceVO.IOS){
				Alert.show("You selected an ipa file which needs to be installed on an Apple device.", "Error");
				_consoleController.log("You selected an ipa file which needs to be installed on an Apple device.", ColorUtils.RED);
				return false;
			}
			
			return true;
		}
		
		private function executeDeploy():void{
			trace("executeDeploy");
			
			//Install
			//iOS
			if(DeviceVO(_application.remoteDeployComponent.deviceComboBox.selectedItem).type == DeviceVO.IOS){
				_commandExecutor.executeCommand(StringUtils.replaceNumberedTokens(CommandConstants.INSTALL_IPA, [DeviceVO(_application.remoteDeployComponent.deviceComboBox.selectedItem).serial, _binary.nativePath]));
			}else{//Android
				var reinstallFlag:String = (_application.remoteDeployComponent.reinstallChkBox.selected) ? "-r" : "";
				
				if(_application.remoteDeployComponent.connectionComboBox.selectedIndex == 1){//WIFI
					_commandExecutor.executeCommand(CommandConstants.WAIT_FOR_DEVICE);
					_commandExecutor.executeCommand(StringUtils.replaceNumberedTokens(CommandConstants.CONNECT_TO_WIFI, [DeviceVO(_application.remoteDeployComponent.deviceComboBox.selectedItem).ip]));
					_commandExecutor.executeCommand(StringUtils.replaceNumberedTokens(CommandConstants.INSTALL_APK, [reinstallFlag, _binary.nativePath]));
				}else{//USB
					if(ApplicationModel.instance.androidDeviceList.length > 1)
						_commandExecutor.executeCommand(StringUtils.replaceNumberedTokens(CommandConstants.INSTALL_APK_BY_SERIAL, [DeviceVO(_application.remoteDeployComponent.deviceComboBox.selectedItem).serial, reinstallFlag, _binary.nativePath]));
					else
						_commandExecutor.executeCommand(StringUtils.replaceNumberedTokens(CommandConstants.INSTALL_APK, [reinstallFlag, _binary.nativePath]));
				}
			}
			
			SharedObjectManager.instance.save(SharedObjectKeys.REMOTE_REINSTALL_FLAG, _application.remoteDeployComponent.reinstallChkBox.selected);
		}
		
		private function loadDevices():void{
			//Load configs
			var configFile:File = File.applicationStorageDirectory.resolvePath("devices.json");
			var fileStream:FileStream = new FileStream(); 
			fileStream.open(configFile, FileMode.READ);
			var fileContent:String = fileStream.readUTFBytes(fileStream.bytesAvailable);
			var configs:Object = (fileContent != "") ? JSON.parse(fileContent) : new Object();
			var configsArray:Array = [];
			
			for each (var device:Object in configs){
				var vo:DeviceVO = new DeviceVO(device.id, device.ip, device.type, device.serial);
				configsArray.push(vo);
			}
			
			_application.remoteDeployComponent.deviceComboBox.dataProvider = new ArrayList(configsArray);
			_consoleController.log("Devices Loaded", ColorUtils.GREEN);
		}
		
		private function downloadAPK():void{
			var url:String = _application.remoteDeployComponent.binaryInput.text;
			
			if(!URLValidation.validate(url)){
				Alert.show("The URL you entered is invalid.", "Error");
				return;
			}
			
			_binary = new File();
			_binary.addEventListener(Event.COMPLETE, onAPKDownloaded);
			_binary.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			_binary.download(new URLRequest(url), "tempAPK.apk");
		}
		
		//Handlers
		public function onDeploy(event:MouseEvent):void{
			trace("deploy");
			downloadAPK();
		}
		
		public function onDeployMultiple(event:MouseEvent):void{
			trace("deploy multiple");
			_consoleController.log("Validating deploy options ...", ColorUtils.BLUE);
			
			_consoleController.log("Deploying package ...", ColorUtils.BLUE);
			
			//executeMultipleDeploy();
		}
		
		public function onSelectConnection(event:IndexChangeEvent):void{
			trace("Selected connection type : "+_application.remoteDeployComponent.connectionComboBox.selectedItem);
			
			if(_application.remoteDeployComponent.connectionComboBox.selectedItem == "USB"){
				if(_application.remoteDeployComponent.binaryInput.text.indexOf("ipa")>-1){
					ApplicationModel.instance.listConnectediOSDevices();	
				}else if(_application.remoteDeployComponent.binaryInput.text.indexOf("apk")>-1){
					ApplicationModel.instance.listConnectedAndroidDevices();	
				}
			}else{
				loadDevices();
			}
		}
		
		public function onSelectConnection2(event:IndexChangeEvent):void{
			if(_application.remoteDeployComponent.connectionComboBox.selectedItem == "USB"){
				ApplicationModel.instance.listConnectediOSDevices();	
				ApplicationModel.instance.listConnectedAndroidDevices();
			}
		}
		
		public function onSelectDevice(event:IndexChangeEvent):void{
			super.onDeviceSelected(_application.remoteDeployComponent.deviceComboBox.selectedItem);
		}
		
		public function onHelpADB(event:MouseEvent):void{
			_commandExecutor.executeCommand(CommandConstants.ADB_HELP);	
		}
		
		public function onHelpADT(event:MouseEvent):void{
			_commandExecutor.executeCommand(CommandConstants.ADT_HELP);
		}
		
		public function onCustomCommandClick(event:MouseEvent):void{
			var customCommandPopup:CustomCommandPopup = new CustomCommandPopup();
			customCommandPopup.commandFunction = _commandExecutor.executeCommand;
			PopUpManager.addPopUp(customCommandPopup, _application, false);
			PopUpManager.centerPopUp(customCommandPopup);
		}
		
		public function onResetConfig(event:MouseEvent):void{
			_application.remoteDeployComponent.deviceComboBox.selectedIndex = -1;
			_application.remoteDeployComponent.binaryInput.text = "";
			_application.remoteDeployComponent.connectionComboBox.selectedIndex = -1;
		}
		
		private function onAPKDownloaded(event:Event):void{
			_consoleController.log("Validating deploy options ...", ColorUtils.BLUE);
			if(!validate())
				return;
			
			_consoleController.log("Deploying package ...", ColorUtils.BLUE);
			SharedObjectManager.instance.save(SharedObjectKeys.LAST_DEPLOYED_REMOTE_BINARY, _application.remoteDeployComponent.binaryInput.text);
			
			executeDeploy();
		}
		
		private function ioErrorHandler(event:IOErrorEvent):void{
			trace("ioErrorHandler");
		}
		
	}
}