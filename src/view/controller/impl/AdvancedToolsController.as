package view.controller.impl
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	
	import Utils.ColorUtils;
	
	import command.CommandConstants;
	
	import controller.ApplicationController;
	import view.controller.BaseTabController;

	public class AdvancedToolsController extends BaseTabController
	{
		public function AdvancedToolsController(applicationController:ApplicationController)
		{
			super(applicationController);
		}
		
		//Public methods
		override public function update():void{
		}
		
		//Private methods
		private function onConfirmReboot(event:CloseEvent):void{
			if(event.detail == Alert.YES){
				_applicationController.commandExecutor.executeCommand(CommandConstants.REBOOT_DEVICE);
			}
		}
		
		private function onConfirmRebootBootloader(event:CloseEvent):void{
			if(event.detail == Alert.YES){
				_applicationController.commandExecutor.executeCommand(CommandConstants.REBOOT_BOOTLOADER);
			}
		}
		
		private function onConfirmRebootRecovery(event:CloseEvent):void{
			if(event.detail == Alert.YES){
				_applicationController.commandExecutor.executeCommand(CommandConstants.REBOOT_RECOVERY);
			}
		}
		
		//Handlers
		public function onClickReboot(event:MouseEvent):void{
			Alert.show("Are you sure you want to reboot the device ?", "Reboot confirmation", Alert.YES|Alert.NO, null, onConfirmReboot);
		}
		
		public function onClickRebootBootloader(event:MouseEvent):void{
			Alert.show("Are you sure you want to reboot the device to the bootloader ?", "Reboot confirmation", Alert.YES|Alert.NO, null, onConfirmRebootBootloader);
		}
		
		public function onClickRebootRecovery(event:MouseEvent):void{
			Alert.show("Are you sure you want to reboot the device to the recovery ?", "Reboot confirmation", Alert.YES|Alert.NO, null, onConfirmRebootRecovery);
		}
	}
}