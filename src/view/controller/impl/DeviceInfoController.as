package view.controller.impl
{
	import Utils.ColorUtils;
	
	import com.pafoid.mobile.data.DeviceVO;
	import com.pafoid.string.StringUtils;
	
	import command.CommandConstants;
	
	import controller.ApplicationController;
	
	import events.DeploymentEvent;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	import model.ApplicationModel;
	
	import spark.events.IndexChangeEvent;
	
	import view.controller.BaseTabController;

	public class DeviceInfoController extends BaseTabController
	{
		private static const BUILD_PROP_FILE:String = "/system/build.prop";
		private static const TEMP_FILE:String = '"'+File.applicationDirectory.nativePath+'\\build.prop"';
	
		public function DeviceInfoController(applicationController:ApplicationController)
		{
			super(applicationController);
			_applicationController.commandHandler.addEventListener(DeploymentEvent.FILE_TRANSFERED, onFileTransfered, false, 0, true);
		}
		
		//Public funtions
		override public function update():void{
			if(ApplicationModel.instance.androidDeviceList && _applicationController.application.deviceInfoComponent.deviceComboBox){
				_applicationController.application.deviceInfoComponent.deviceComboBox.dataProvider = null;
				_applicationController.application.deviceInfoComponent.deviceComboBox.dataProvider = ApplicationModel.instance.androidDeviceList;
			}
		}
		
		public function getInfos(event:IndexChangeEvent = null):void{
			trace("getInfos");
			_applicationController.setStatusText("Retrieving Device Infos");
			
			ApplicationModel.instance.currentDeviceVO = _applicationController.application.deviceInfoComponent.deviceComboBox.selectedItem as DeviceVO;
			if(ApplicationModel.instance.currentDeviceVO){
				_applicationController.commandHandler.addEventListener(DeploymentEvent.FILE_TRANSFERED, onFileTransfered, false, 0, true);
				_applicationController.commandExecutor.executeCommand(StringUtils.replaceNumberedTokens(CommandConstants.PULL_FILE, [ApplicationModel.instance.currentDeviceVO.serial, BUILD_PROP_FILE, TEMP_FILE]));
			}
		}
		
		//Handlers
		private function onFileTransfered(event:DeploymentEvent):void{
			_applicationController.commandHandler.removeEventListener(DeploymentEvent.FILE_TRANSFERED, onFileTransfered);
			_applicationController.setStatusText("Retrieved build.prop");
			
			var loader:URLLoader = new URLLoader();
			loader.addEventListener(Event.COMPLETE, onFileLoaded, false, 0, true);
			loader.load(new URLRequest(File.applicationDirectory.nativePath+"\\build.prop"));
		}
		
		private function onFileLoaded(event:Event):void{
			event.target.removeEventListener(Event.COMPLETE, onFileLoaded);
			_applicationController.setStatusText("Parsing build.prop");
			
			var content:String = event.target.data;	
			var start:int;
			var end:int;
			
			start = content.indexOf("ro.build.id=") + 12;
			end = start + 6;
			var buildNumber:String = content.substring(start, end);
			
			start = content.indexOf("ro.product.model=") + 17;
			end = content.indexOf("\n", start);
			var modelName:String = content.substring(start, end);
			
			start = content.indexOf("ro.product.brand=") + 17;
			end = content.indexOf("\n", start);
			var brandName:String = content.substring(start, end);
			
			start = content.indexOf("ro.product.manufacturer=") + 24;
			end = content.indexOf("\n", start);
			var manufacturer:String = content.substring(start, end);

			start = content.indexOf("ro.build.version.release=") + 25;
			end = content.indexOf("\n", start);
			var androidVersion:String = content.substring(start, end);
			
			start = content.indexOf("ro.product.cpu.abi=") + 19;
			end = content.indexOf("\n", start);
			var cpu:String = content.substring(start, end);
			
			ApplicationModel.instance.currentDeviceVO.buildNumber = buildNumber;
			ApplicationModel.instance.currentDeviceVO.modelName = modelName;
			ApplicationModel.instance.currentDeviceVO.brandName = brandName;
			ApplicationModel.instance.currentDeviceVO.manufacturer = manufacturer;
			ApplicationModel.instance.currentDeviceVO.androidVersion = androidVersion;
			ApplicationModel.instance.currentDeviceVO.cpu = cpu;
			
			if(_applicationController.application.deviceInfoComponent.buildVersion){
				_applicationController.application.deviceInfoComponent.buildVersion.text = buildNumber;
				_applicationController.application.deviceInfoComponent.cpu.text = cpu;
				_applicationController.application.deviceInfoComponent.brand.text = brandName;
				_applicationController.application.deviceInfoComponent.model.text = modelName;
				_applicationController.application.deviceInfoComponent.androidVersion.text = androidVersion;
				_applicationController.application.deviceInfoComponent.manufacturer.text = manufacturer;
				
				_applicationController.application.deviceInfoComponent.ip.text = ApplicationModel.instance.currentDeviceVO.ip;
				_applicationController.application.deviceInfoComponent.serial.text = ApplicationModel.instance.currentDeviceVO.serial;
				
				_applicationController.application.deviceInfoComponent.deviceComboBox.dataProvider = null;
				_applicationController.application.deviceInfoComponent.deviceComboBox.dataProvider = ApplicationModel.instance.androidDeviceList;
			}			
			
			_applicationController.setStatusText("Device Infos Retrieved");
			
			_applicationController.commandHandler.addEventListener(DeploymentEvent.ANDROID_DEVICES_LISTED, ApplicationModel.instance.onListAndroidDevices);
			_applicationController.commandHandler.dispatchEvent(new DeploymentEvent(DeploymentEvent.ANDROID_DEVICES_LISTED, ApplicationModel.instance.androidDeviceList));
		}
	}
}