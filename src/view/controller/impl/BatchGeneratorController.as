package view.controller.impl
{
	import com.pafoid.sharedObject.SharedObjectManager;
	
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.FileFilter;
	import flash.utils.ByteArray;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	
	import Utils.ColorUtils;
	
	import controller.ApplicationController;
	
	import data.SharedObjectKeys;
	
	import deng.fzip.FZip;
	import deng.fzip.FZipEvent;
	import deng.fzip.FZipFile;
	
	import view.controller.BaseTabController;

	public class BatchGeneratorController extends BaseTabController
	{
		private const ECHO_OFF:String = "@ECHO OFF"+File.lineEnding;
		private const DISCLAIMER:String = "ECHO This batch file was created by the MobileAppDeployer"+File.lineEnding;
		
		private var _binaryFocused:Boolean = false;
		private var _binary:File;
		private var _appId:String = "";
		private var _appName:String = "";
		
		private var _batchString:String = "";
		private var _filesDP:ArrayCollection = new ArrayCollection();
		private var _files:Array = [];
		
		private var _batch:File;
		
		//Options
		private var _reinstall:Boolean;
		private var _onSdCard:Boolean;
		private var _echoOff:Boolean;
		
		public function BatchGeneratorController(applicationController:ApplicationController)
		{
			super(applicationController);
		}
		
		//Public methods
		override public function update():void{
			//Last batch file
			if(_application.batchGeneratorComponent.binaryInput3){
				_application.batchGeneratorComponent.binaryInput3.text = SharedObjectManager.instance.getStringValue(SharedObjectKeys.LAST_GENERATED_BATCH);
				
				if(_application.batchGeneratorComponent.binaryInput3.text != ""){
					_binary = new File().resolvePath(_application.batchGeneratorComponent.binaryInput3.text);
					getAppId();
					
					_application.batchGeneratorComponent.echoChkBox.selected = SharedObjectManager.instance.getValue(SharedObjectKeys.ECHO_BATCH);
					_application.batchGeneratorComponent.reinstallChkBox.selected = SharedObjectManager.instance.getValue(SharedObjectKeys.REINSTALL_BATCH);
					_application.batchGeneratorComponent.sdCardChkBox.selected = SharedObjectManager.instance.getValue(SharedObjectKeys.INSTALL_ON_SD_BATCH);
				}else{
					_application.batchGeneratorComponent.binaryInput3.text = "Click to select binary file";
				}
			}
		}
		
		//Handlers
		public function onBinaryFocus(event:FocusEvent):void{
			if(!_binaryFocused){
				_binary = new File();
				_binary.addEventListener(Event.SELECT, onSelectBinary);
				_binary.addEventListener(Event.CANCEL, onCancelBinary);
				_binary.browse([new FileFilter("Android/iOS Application Package", "*.apk;*.ipa")]);
				
				_binaryFocused = true;
				
				_application.status = "Select Binary To Deploy";
			}
		}
		
		private function onSelectBinary(event:Event):void{
			_binaryFocused = false;
			_application.batchGeneratorComponent.binaryInput3.text = event.target.nativePath;
			_application.vs.setFocus();
			_consoleController.log("Selected binary : "+_binary.name, ColorUtils.BLUE);
			
			_application.status = "Getting App Id";
			
			getAppId();
		}
		
		private function onCancelBinary(event:Event):void{
			trace("cancelHandler: " + event);
			_binaryFocused = false;
			_application.vs.setFocus();
		}
		
		private function getAppId():void{
			if(_binary == null || !_binary.exists)
				return;
			
			var totalFilesSize:Number = 0;
			var startTime:Number = new Date().time;
			var zipFileBytes:ByteArray = new ByteArray();
			var fs:FileStream = new FileStream();
			fs.open(_binary, FileMode.READ);
			fs.readBytes(zipFileBytes);
			fs.close();
			var fzip:FZip = new FZip();
			fzip.addEventListener(FZipEvent.FILE_LOADED, onFileLoaded);
			fzip.addEventListener(Event.COMPLETE, onUnzipComplete);
			fzip.loadBytes(zipFileBytes);
		}
		
		private function onFileLoaded(e:FZipEvent):void{
			_application.status = "Binary Loaded";
			
			var fzf:FZipFile = e.file;
			_files.push(fzf);
			if (fzf.sizeUncompressed == 0) return;
			
			var fileData:Object = new Object();
			fileData.name = fzf.filename;
			_filesDP.addItem(fileData);
		}
		
		private function onUnzipComplete(e:Event):void{
			_application.status = "Binary Unzipped";
			
			var fzip:FZip = e.target as FZip;
			fzip.removeEventListener(FZipEvent.FILE_LOADED, onFileLoaded);
			fzip.removeEventListener(Event.COMPLETE, onUnzipComplete);
			
			var appDescriptor:FZipFile;
			var file:FZipFile;
			for each(file in _files){
				if(file.filename.indexOf("application.xml") > -1){
					appDescriptor = file;
				}
			}
			
			var descriptorContent:String = appDescriptor.content.readUTFBytes(appDescriptor.content.bytesAvailable);
			var xml:XMLList = new XMLList(descriptorContent).children();
			_appId = xml[0];
			_appName = xml.children()[2];
		}
		
		public function generateBatchFile(event:MouseEvent):void{
			trace("generateBatchFile");
			_application.status = "Generating BatchFile";
			
			if(!_binary){
				if(_application.batchGeneratorComponent.binaryInput3.text != ""){
					_binary = new File().resolvePath(_application.batchGeneratorComponent.binaryInput3.text);
					getAppId();
					if(!_binary){
						Alert.show("Binary file could not be loaded.", "Error");
						return;
					}
				}else{
					Alert.show("No binary file selected", "Error");
					//throw new Error("No binary file selected");
					return;					
				}
			}
			
			_batchString = "";
			if(_binary.extension == "ipa"){
				generateiOSBatchFile();
			}else if(_binary.extension == "apk"){
				generateAndroidBatchFile();
			}
		}
		
		private function generateiOSBatchFile():void{
			trace("generateiOSBatchFile");
			if(_echoOff)
				_batchString = ECHO_OFF;
			
			_batchString += DISCLAIMER;
			var action:String = (_reinstall) ? "reinstall " : "install ";
			_batchString += "ECHO This will "+action+_appName+" for iOS on connected device."+File.lineEnding;
			
			_batchString += "cd "+_binary.parent.nativePath+" "+File.lineEnding;
			
			if(_reinstall){
				_batchString += "adt -uninstallApp -platform ios -appid "+_appId+File.lineEnding;
				_batchString += "PAUSE"+File.lineEnding;
			}
			
			_batchString += 'adt -installApp -platform ios -package "'+_binary.name+'"'+File.lineEnding;						
			_batchString += "PAUSE"+File.lineEnding;
			
			saveBatch("ios");
		}
		
		private function generateAndroidBatchFile():void{
			trace("generateAndroidBatchFile");
			if(_echoOff)
				_batchString = ECHO_OFF;		
			
			_batchString += DISCLAIMER;
			var action:String = (_reinstall) ? "reinstall " : "install ";
			_batchString += "ECHO This will "+action+_appName+" for Android on connected device."+File.lineEnding;
			
			_batchString += "cd "+_binary.parent.nativePath+" "+File.lineEnding;
			_batchString += "adb install ";
			
			if(_reinstall){
				_batchString += "-r ";
			}
			
			if(_onSdCard){
				_batchString += "-s ";
			}
			
			_batchString += '"'+_binary.name+'"'+File.lineEnding;						
			_batchString += "PAUSE"+File.lineEnding;
			
			saveBatch("android");
		}
		
		private function saveBatch(platform:String):void{
			var fileName:String = _appName+"-"+platform+".bat";
			var illegalCharacters:Array = [':', '/', '|', '\\', '*', '"', '?', '<', '>'];
			for each (var illegalCharacter:String in illegalCharacters) 
			{
				if(fileName.indexOf(illegalCharacter) >= 0){
					fileName = "batch.bat";
					continue;
				}
			}
			
			_batch = File.desktopDirectory.resolvePath(fileName);
			_batch.addEventListener(Event.SELECT, onSelectFileLocation);
			_batch.browseForSave("Save Batch File for "+_appName+" on "+platform);
			
			_application.status = "Saving BatchFile";
		}
		
		protected function onSelectFileLocation(event:Event):void{
			var fileStream:FileStream = new FileStream();
			fileStream.open(_batch, FileMode.WRITE);
			fileStream.writeUTFBytes(_batchString);
			fileStream.close();
			
			SharedObjectManager.instance.save(SharedObjectKeys.LAST_GENERATED_BATCH, _application.batchGeneratorComponent.binaryInput3.text);
			SharedObjectManager.instance.save(SharedObjectKeys.ECHO_BATCH, _application.batchGeneratorComponent.echoChkBox.selected);
			SharedObjectManager.instance.save(SharedObjectKeys.REINSTALL_BATCH, _application.batchGeneratorComponent.reinstallChkBox.selected);
			SharedObjectManager.instance.save(SharedObjectKeys.INSTALL_ON_SD_BATCH, _application.batchGeneratorComponent.sdCardChkBox.selected);
			
			_application.status = "BatchFile Saved";
		}
		
		public function onCheckReinstall(event:Event):void{
			_reinstall = _application.batchGeneratorComponent.reinstallChkBox.selected;
		}
		
		public function onCheckSD(event:Event):void{
			_onSdCard = _application.batchGeneratorComponent.sdCardChkBox.selected;
		}
		
		public function onCheckEcho(event:Event):void{
			_echoOff = !_application.batchGeneratorComponent.echoChkBox.selected;
		}
	}
}