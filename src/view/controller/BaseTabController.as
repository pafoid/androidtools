package view.controller
{
	import Utils.ColorUtils;
	
	import com.pafoid.mobile.data.DeviceVO;
	
	import command.CommandConstants;
	import command.CommandExecutor;
	import command.CommandHandler;
	
	import controller.ApplicationController;
	import controller.ConsoleController;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	
	import model.ApplicationModel;
	
	import spark.events.IndexChangeEvent;

	public class BaseTabController
	{
		protected var _application:AndroidTools;
		protected var _applicationController:ApplicationController;
		protected var _consoleController:ConsoleController;
		protected var _commandExecutor:CommandExecutor;
		protected var _commandHandler:CommandHandler;
		
		protected var _exportFile:File;
		
		public function BaseTabController(applicationController:ApplicationController)
		{
			_application = applicationController.application;
			_applicationController = applicationController;
			_consoleController = applicationController.consoleController;
			_commandExecutor = applicationController.commandExecutor;
			_commandHandler = applicationController.commandHandler;
			
			update();
		}
		
		//Public functions
		public function update():void{
			trace("BaseTabController.update()::Calling update() on a method that should be overriden");
		}
		
		//Handlers
		public function onClearConsole(event:MouseEvent):void{
			_consoleController.clearConsole();
		}
		
		public function onExport(event:MouseEvent):void{
			var content:String = _applicationController.consoleController.console.htmlText;
			
			_exportFile = new File();
			_exportFile.addEventListener(Event.CANCEL, onCancelExport)
			_exportFile.addEventListener(Event.COMPLETE, onExportComplete)
			_exportFile.save(content, "console.html");
		}		
		
		protected function onExportComplete(event:Event):void{
			trace("Console exported");
			_applicationController.consoleController.log("Console Exported", ColorUtils.GREEN);
		}
		
		protected function onCancelExport(event:Event):void{
			trace("Export Canceled");
			_applicationController.consoleController.log("Export Canceled", ColorUtils.RED);
		}
		
		public function onClickDetect(event:MouseEvent):void{
			_commandExecutor.executeCommand(CommandConstants.LIST_ANDROID_DEVICES);
		}
		
		public function onDeviceSelected(vo:DeviceVO):void{
			ApplicationModel.instance.currentDeviceVO = vo;
			_commandHandler.hasRoot = false;
		}
	}
}