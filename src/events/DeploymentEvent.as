package events
{
	import flash.events.Event;
	
	public class DeploymentEvent extends Event
	{
		public static const ANDROID_DEVICES_LISTED:String = "AndroidDevicesListed";
		public static const IOS_DEVICES_LISTED:String = "iOSDevicesListed";
		public static const FILE_TRANSFERED:String = "fileTransfered";
		
		public var data:*;
		
		public function DeploymentEvent(type:String, data:*=null)
		{
			this.data = data;
			super(type, true, false);
		}
	}
}