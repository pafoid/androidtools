package command
{
	import flash.filesystem.File;

	public class CommandConstants
	{
		public static const LIST_ANDROID_DEVICES:String = "adb devices"+File.lineEnding;
		public static const LIST_IOS_DEVICES:String = "adt -devices -platform ios"+File.lineEnding;
		
		public static const IP_ADDRESS_COMMAND_BY_SERIAL:String = "adb -s {0} shell ifconfig wlan0"+File.lineEnding;
		public static const IP_ADDRESS_COMMAND:String = "adb shell ifconfig wlan0"+File.lineEnding;
		
		public static const START_SCREEN_RECORD:String = 'adb -s {0} shell screenrecord --size {1}x{2} --bit-rate {3} --time-limit {4}{5}{6} "sdcard/{7}"'+File.lineEnding;
		public static const SHOW_SCREEN_RECORD_HELP:String = "adb shell screenrecord --help"+File.lineEnding;
		
		public static const WAIT_FOR_DEVICE:String = "adb wait-for-device"+File.lineEnding;
		public static const CONNECT_TO_WIFI:String = "adb connect {0}"+File.lineEnding;
		
		public static const INSTALL_APK:String = "adb install {0} {1}"+File.lineEnding;
		public static const INSTALL_APK_BY_SERIAL:String = 'adb -s {0} install {1} "{2}"'+File.lineEnding;
		
		public static const INSTALL_IPA:String = 'adt -installApp -platform ios -device {0} -package "{1}"'+File.lineEnding;
		
		public static const ADB_HELP:String = "adb help"+File.lineEnding
		public static const ADT_HELP:String = "adt -help"+File.lineEnding;
		
		public static const LIST_PACKAGES:String = "adb shell pm list packages -f -3"+File.lineEnding;
		public static const UNINSTALL_PACKAGE:String = "adb shell pm uninstall {0}"+File.lineEnding;
		
		public static const LOG_CAT:String = "adb logcat"+File.lineEnding;
		
		public static const UP_TIME:String = "adb shell uptime"+File.lineEnding;
		public static const FREE_SPACE:String = "adb shell df"+File.lineEnding;
		public static const PULL_FILE:String = "adb -s {0} pull {1} {2}"+File.lineEnding;
		public static const DELETE_FILE:String = "adb -s {0} shell rm {1}"+File.lineEnding;
		
		public static const REBOOT_DEVICE:String = "adb reboot"+File.lineEnding;
		public static const REBOOT_BOOTLOADER:String = "adb reboot bootloader"+File.lineEnding;
		public static const REBOOT_RECOVERY:String = "adb reboot recovery"+File.lineEnding;
		
		public static const DETECT_ROOT_ACCESS:String = "adb shell su"+File.lineEnding;
		
		public static const SCREEN_RESET_SIZE:String = "adb shell wm size reset"+File.lineEnding;
		public static const SCREEN_RESET_DENSITY:String = "adb shell wm density reset"+File.lineEnding;
		
		public static const SCREEN_SET_SIZE:String = "adb shell wm size {0}x{1}"+File.lineEnding;
		public static const SCREEN_SET_DENSITY:String = "adb shell wm density {0}"+File.lineEnding;
		
		public static const SCREEN_ROOT_SET_SIZE:String = "adb shell su wm size {0}x{1}"+File.lineEnding;
		public static const SCREEN_ROOT_SET_DENSITY:String = "adb shell su wm density {0}"+File.lineEnding;
		
		//public static const EXIT:String = "exit"+File.lineEnding; 
		public static const EXIT:String = "^C"+File.lineEnding;
	}
}