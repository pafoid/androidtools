package command
{
	import flash.desktop.NativeProcess;
	import flash.desktop.NativeProcessStartupInfo;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.filesystem.File;
	import flash.utils.setTimeout;
	
	import Utils.ColorUtils;

	public class CommandExecutor extends EventDispatcher
	{
		private var _process:NativeProcess;
		private var _log:Function;
		private var _shellInitialized:Boolean;
		
		private var _commandHandler:CommandHandler;
		private var _lastCommand:String;
		
		public function CommandExecutor(logFunction:Function){
			_log = logFunction;
		}
		
		public function initShell():void{
			var nativeProcessStartupInfo:NativeProcessStartupInfo = new NativeProcessStartupInfo(); 
			var file:File = File.applicationDirectory.resolvePath("C:\\Windows\\System32\\cmd.exe");
			nativeProcessStartupInfo.executable = file;
			_process = new NativeProcess();
			
			_process.addEventListener(ProgressEvent.STANDARD_INPUT_PROGRESS, _commandHandler.inputDataHandler);
			_process.addEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, _commandHandler.onOutputData, false, 1000);
			
			_process.addEventListener(ProgressEvent.STANDARD_ERROR_DATA, _commandHandler.errorHandler, false, -1000);
			_process.addEventListener(IOErrorEvent.STANDARD_ERROR_IO_ERROR, _commandHandler.errorHandler, false, -1000);
			
			//_process.addEventListener(Event.DEACTIVATE, _commandHandler.onProcessClose, false, 1000);
			
			_process.start(nativeProcessStartupInfo);
			
			dispatchEvent(new ShellEvent(ShellEvent.SHELL_INITIALIZED));
			_shellInitialized = true;
		}		
		
		public function closeShell(callback:Function = null):void{
			_process.closeInput();
			_process.exit(true);
			
			_process.removeEventListener(ProgressEvent.STANDARD_INPUT_PROGRESS, _commandHandler.inputDataHandler);
			_process.removeEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, _commandHandler.onOutputData);
			
			_process.removeEventListener(ProgressEvent.STANDARD_ERROR_DATA, _commandHandler.errorHandler);
			_process.removeEventListener(IOErrorEvent.STANDARD_ERROR_IO_ERROR, _commandHandler.errorHandler);
			
			_shellInitialized = false;
			
			dispatchEvent(new ShellEvent(ShellEvent.SHELL_CLOSED));
			
			if(callback != null){
				callback();
			}
		}
		
		//General
		public function executeCommand(command:String):void{
			if(!_shellInitialized){
				initShell();
				setTimeout(executeCommand, 3000, command);
				return;
			}
			
			trace("executeCommand : ", command);
			
			if(_process && _process.running){
				_lastCommand = command;
				_process.standardInput.writeUTFBytes(command);
				_log(command, ColorUtils.ORANGE);
			}else{
				trace("Command cannot be executed.");
				_log("Error, command cannot be executed. ADB stopped running", ColorUtils.RED);
			}
		}
		
		public function stopListeningForOutput():void{
			_process.removeEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, _commandHandler.onOutputData);
		}
		
		public function startListeningForOutput():void{
			_process.addEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, _commandHandler.onOutputData, false, 1000);
		}
		
		public function changeOutputListener(listener:Function):void{
			stopListeningForOutput();
			_process.addEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, listener, false, 1000);
		}
		
		public function resetOutputListener(listener:Function):void{
			_process.removeEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, listener);
			startListeningForOutput();
		}
		
		//Getters/Setters
		public function set log(value:Function):void
		{
			_log = value;
		}
		
		public function get process():NativeProcess
		{
			return _process;
		}
		
		//Public methods
		public function destroy():void{
			_process.closeInput();
			_process.exit(true);
		}

		public function get commandHandler():CommandHandler
		{
			return _commandHandler;
		}

		public function set commandHandler(value:CommandHandler):void
		{
			_commandHandler = value;
		}

		public function get lastCommand():String
		{
			return _lastCommand;
		}

		public function get shellInitialized():Boolean
		{
			return _shellInitialized;
		}

		public function set shellInitialized(value:Boolean):void
		{
			_shellInitialized = value;
		}


	}
}