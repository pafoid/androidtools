package command
{
	import com.pafoid.array.ArrayUtils;
	import com.pafoid.mobile.data.DeviceVO;
	import com.pafoid.mobile.data.PackageVO;
	import com.pafoid.string.StringUtils;
	
	import flash.desktop.NativeProcess;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.NativeProcessExitEvent;
	import flash.events.ProgressEvent;
	import flash.utils.clearTimeout;
	import flash.utils.setTimeout;
	
	import mx.collections.ArrayList;
	import mx.controls.Alert;
	
	import Utils.ColorUtils;
	
	import controller.ApplicationController;
	
	import events.DeploymentEvent;
	
	import model.ApplicationModel;
	
	public class CommandHandler extends EventDispatcher
	{
		private var _applicationController:ApplicationController;
		private var _commandExecutor:CommandExecutor;
		private var _logFunction:Function;
		
		private var _packageListString:String = "";
		private var _msgString:String = "";
		private var _screenRecordHelpString:String = "";
		private var _callback:Function;
		private var _androidDeviceArray:Array = [];
		
		private var _timeOut:uint;
		
		public var hasRoot:Boolean;
		
		public function CommandHandler(applicationController:ApplicationController, logFunction:Function)
		{
			_applicationController = applicationController;
			_commandExecutor = _applicationController.commandExecutor;
			_logFunction = logFunction;
		}
		
		//Private methods
		
		//Custom Commands
		public function checkRootAccess():void{
			_applicationController.setStatusText("Checking root access ...");
			
			_commandExecutor.changeOutputListener(onCheckForRootAccess);
			_commandExecutor.executeCommand(CommandConstants.DETECT_ROOT_ACCESS);
		}
		
		public function listConnectedAndroidDevices():void{
			_commandExecutor.executeCommand(CommandConstants.LIST_ANDROID_DEVICES);
		}
		
		public function listConnectediOSDevices():void{
			_commandExecutor.executeCommand(CommandConstants.LIST_IOS_DEVICES);
		}
		
		public function abortCommand():void{
			trace("abort does not work yet");
		}
		
		//Handlers
		public function inputDataHandler(event:ProgressEvent):void{
			//trace("inputDataHandler - closing input");
			//process.closeInput();
		}
		
		public function onOutputData(event:ProgressEvent):void{
			var msg:String = NativeProcess(event.target).standardOutput.readUTFBytes(_commandExecutor.process.standardOutput.bytesAvailable);
			trace("Got: ", msg);
			
			if(msg.indexOf("List of devices attached \r\n")>-1){				
				getIpAddresses(msg);
			}else if(msg.indexOf("List of attached devices:\r\n")>-1){
				trace("device : "+msg.substr(78, 4));
				var iosDeviceList:Array = [new DeviceVO(msg.substr(78, 4), "", DeviceVO.IOS, msg.substr(76, 1))];
				dispatchEvent(new DeploymentEvent(DeploymentEvent.IOS_DEVICES_LISTED, iosDeviceList));
				_logFunction("Connected iOS Devices Listed !", ColorUtils.GREEN);
			}else if(msg.indexOf("wlan0") > -1){
				_msgString += msg;
				
				waitForCommandToFinish(onIpAddressesListed);
			}else if(msg.indexOf("is not recognized as an internal or external command, operable program or batch file.") > -1 && msg.indexOf("'adb'") > -1){
				Alert.show("The Android Debug Bridge (ADB) is not installed on your computer or an environment variable is not set to point to the Android SDK", "ADB not found"); 
			}else if(msg.indexOf("is not recognized as an internal or external command, operable program or batch file.") > -1 && msg.indexOf("'adt'") > -1){
				Alert.show("ADT is not installed on your computer or an environment variable is not set to point to the Android SDK", "ADT not found"); 
			}else if(msg.indexOf("Success")>-1){
				_logFunction(msg, ColorUtils.GREEN);
				var lc:String = _commandExecutor.lastCommand;
				if(_applicationController.application.packageManagerComponent.packageList && 
					_applicationController.application.packageManagerComponent.packageList.selectedItem && 
					lc == StringUtils.replaceNumberedTokens(CommandConstants.UNINSTALL_PACKAGE, [_applicationController.application.packageManagerComponent.packageList.selectedItem.name])){
					_applicationController.setStatusText("Package "+_applicationController.application.packageManagerComponent.packageList.selectedItem.name+" uninstalled");
					_commandExecutor.executeCommand(CommandConstants.LIST_PACKAGES);
				}
			}else if(msg.indexOf("package:")>-1){
				_packageListString += msg;
				waitForCommandToFinish(parsePackagesString);
			}else if(msg == "\r\nC:\\Program Files\\Adobe\\Adobe Flash Builder 4.7 (64 Bit)>" && ApplicationModel.instance.androidDeviceList.length == 0 && _commandExecutor.lastCommand == CommandConstants.WAIT_FOR_DEVICE){
				trace("wait complete");
				_commandExecutor.executeCommand(CommandConstants.LIST_ANDROID_DEVICES);
			}else if(msg.indexOf("record") > -1){//Screen record help
				_screenRecordHelpString += msg;
				waitForCommandToFinish(delayedLog);
			}else if(msg.indexOf("Failure") > -1){//Failed installation
				_logFunction(msg, ColorUtils.RED);
			}else{
				//_log(msg, ColorUtils.GREY);
			}
		}
		
		public function errorHandler(event:Event):void{
			var msg:String = NativeProcess(event.target).standardOutput.readUTFBytes(_commandExecutor.process.standardOutput.bytesAvailable);
			var error:String = NativeProcess(event.target).standardError.readUTFBytes(_commandExecutor.process.standardError.bytesAvailable);
			
			trace("msg", msg);
			trace("error", error);
			
			if(error.indexOf("Android Debug Bridge version") > -1){//ADB help
				error = error.split("\r\n                                 ").join(" ");
				error = error.split("                            ").join("\t");
				error = error.split("\r\n").join("\n");
				error = error.split("          ").join("\t");
				_logFunction(error, ColorUtils.GREY);
			}else if(error.indexOf("KB/s") > -1){//File transfer
				var color:String = (error.indexOf("error") > -1) ? ColorUtils.RED : ColorUtils.GREEN; 
				_logFunction(error, color);
				dispatchEvent(new DeploymentEvent(DeploymentEvent.FILE_TRANSFERED));
			}else{
				_logFunction(error, ColorUtils.RED);
			}
			_logFunction(msg, ColorUtils.YELLOW);
			
			/*setTimeout(log, 500, error, ColorUtils.RED);
			setTimeout(log, 500, msg, ColorUtils.YELLOW);*/
		}
		
		public function onProcessClose(event:Event):void{
			if(_commandExecutor.shellInitialized){
				_logFunction("Connection to the shell was lost, reinitializing ...", ColorUtils.RED);
				_applicationController.setStatusText("Connection to the shell was lost, reinitializing ...");
				
				_commandExecutor.shellInitialized = false;
				_commandExecutor.addEventListener(ShellEvent.SHELL_INITIALIZED, _applicationController.onShellInitialized, false, 0, true);
				_commandExecutor.initShell();			
			}
		}
		
		public function onCheckForRootAccess(event:ProgressEvent):void{
			var msg:String = NativeProcess(event.target).standardOutput.readUTFBytes(_commandExecutor.process.standardOutput.bytesAvailable);
			trace("Got: ", msg);
			
			hasRoot = (msg.indexOf(":/ #") > -1);
			
			_applicationController.consoleController.log("Root access detected !", ColorUtils.GREEN);
			_applicationController.setStatusText("Root access detected");
			_commandExecutor.resetOutputListener(onCheckForRootAccess);
			
			//_commandExecutor.executeCommand(CommandConstants.EXIT);
			_commandExecutor.closeShell(_commandExecutor.initShell);
		}
		
		private function getIpAddresses(msg:String):void{
			_applicationController.setStatusText("Getting IP Addresses");
			
			msg = msg.split("\r\n").join("");
			msg = msg.split("List of devices attached").join("");
			
			_androidDeviceArray = [];
			
			var ipRegEx:RegExp = /[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}:[0-9]{4}/g;
			var serialRegEx:RegExp =  /[a-z0-9]{8,16}/g;
			var emulatorRegEx:RegExp = /emulator-[0-9]{4}/;
			
			var ipArray:Array = ipRegEx.exec(msg);
			
			while(ipArray){
				_androidDeviceArray.push(new DeviceVO("Android Device", ipArray[0], DeviceVO.ANDROID_EMU, ipArray[0]));
				
				ipArray = ipRegEx.exec(msg);
			}
			
			var serialArray:Array = serialRegEx.exec(msg);
			
			while(serialArray){
				_androidDeviceArray.push(new DeviceVO("Android Device", "", DeviceVO.ANDROID_DEVICE, serialArray[0]));
				
				serialArray = serialRegEx.exec(msg);
			}
			
			var emulatorArray:Array = emulatorRegEx.exec(msg);
			
			while(emulatorArray){
				_androidDeviceArray.push(new DeviceVO("Android Device", "", DeviceVO.ANDROID_DEVICE, emulatorArray[0]));
				
				emulatorArray = emulatorRegEx.exec(msg);
			}
			
			for each (var vo:DeviceVO in _androidDeviceArray) 
			{
				if(vo.type == DeviceVO.ANDROID_DEVICE){
					_commandExecutor.executeCommand(StringUtils.replaceNumberedTokens(CommandConstants.IP_ADDRESS_COMMAND_BY_SERIAL, [vo.serial]));
				}
			}
			
			if(_androidDeviceArray.length == 0){
				_logFunction("Warning - No Android Device Detected !", ColorUtils.YELLOW);
				dispatchEvent(new DeploymentEvent(DeploymentEvent.ANDROID_DEVICES_LISTED, []));
			}
		}
		
		private function onIpAddressesListed():void{
			var serialRegEx:RegExp = /[a-z0-9]{8,16}/g;
			var serial:String = serialRegEx.exec(_msgString)[0];
			
			var ipRegEx:RegExp = /[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/;
			var ip:String = ipRegEx.exec(_msgString)[0]; 
			
			for each (var vo:DeviceVO in _androidDeviceArray) 
			{
				if(vo.serial == serial){
					vo.ip = ip;
					continue;
				}
			}
			
			dispatchEvent(new DeploymentEvent(DeploymentEvent.ANDROID_DEVICES_LISTED, _androidDeviceArray));
			
			_msgString = _msgString.split(_commandExecutor.lastCommand).join("");
			_logFunction(_msgString, ColorUtils.ORANGE);
			_msgString = "";
			
			_logFunction("Connected Android Devices Listed !", ColorUtils.GREEN);
			_applicationController.setStatusText("Connected Android Devices Listed");
			_callback = null;
		}
		
		private function waitForCommandToFinish(callback:Function):void{
			_callback = callback;
			_applicationController.application.addEventListener(Event.ENTER_FRAME, isCommandFinished, false, 0, true);
		}
		
		private function isCommandFinished(event:Event):void{
			if(_commandExecutor.process.standardOutput.bytesAvailable == 0){
				_timeOut = setTimeout(isCommandReallyFinished, 750);
			}
		}
		
		private function isCommandReallyFinished():void{
			if(_commandExecutor.process.standardOutput.bytesAvailable == 0){
				clearTimeout(_timeOut);
				
				_applicationController.application.removeEventListener(Event.ENTER_FRAME, isCommandFinished);
				if(_callback != null)
					_callback();
				_callback = null;
			}
		}
		
		private function parsePackagesString():void{
			_applicationController.setStatusText("Parsing package");
			
			var tempArray:Array = [];
					
			var locationRegEx:RegExp = new RegExp("package:([a-zA-Z0-9\\./-]*)=([a-zA-Z0-9\\./-]*)", "gm");
			locationRegEx.lastIndex = 0;
			var packageArray:Array = locationRegEx.exec(_packageListString);
			
			while(packageArray){
				var vo:PackageVO = new PackageVO(packageArray[2], packageArray[1]);
				trace(vo.toString());
				tempArray.push(vo);

				packageArray = locationRegEx.exec(_packageListString);
			}
			
			_packageListString = "";
			
			tempArray.sortOn("name");
			ApplicationModel.instance.currentDeviceVO.packageList = new ArrayList(tempArray);
			_applicationController.application.packageManagerComponent.packageList.dataProvider = ApplicationModel.instance.currentDeviceVO.packageList;
			
			trace("Done listing packages");
			_applicationController.setStatusText("Packages Listed");
		}
		
		private function delayedLog():void{
			_logFunction(_screenRecordHelpString, ColorUtils.BLUE);
		}
	}
}