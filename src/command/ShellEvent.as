package command
{
	import flash.events.Event;
	
	public class ShellEvent extends Event
	{
		public static const SHELL_INITIALIZED:String = "shellInitialized";
		public static const SHELL_CLOSED:String = "shellClosed";
		
		public function ShellEvent(type:String)
		{
			super(type, true, false);
		}
	}
}