package model
{
	import com.pafoid.mobile.data.DeviceVO;
	
	import mx.collections.ArrayList;
	
	import Utils.ColorUtils;
	
	import command.CommandConstants;
	
	import controller.ApplicationController;
	
	import events.DeploymentEvent;

	public class ApplicationModel
	{
		private static var _instance:ApplicationModel = new ApplicationModel();
		
		public var locale:String = "en";
		public var androidDeviceList:ArrayList = new ArrayList();
		public var iosDeviceList:ArrayList;
		public var devicesLoadedOnce:Boolean;
		public var currentDeviceVO:DeviceVO;
		
		public var applicationController:ApplicationController;
		
		public function ApplicationModel()
		{
			if(instance){
				throw new Error ("We cannot create a new instance.Please use Singleton.getInstance()");
			}
		}
		
		//Public methods
		public function listConnectedAndroidDevices():void{
			applicationController.consoleController.log("Listing Connected Android Devices ...", ColorUtils.BLUE);
			
			applicationController.commandHandler.addEventListener(DeploymentEvent.ANDROID_DEVICES_LISTED, onListAndroidDevices);
			applicationController.commandHandler.listConnectedAndroidDevices();
		}
		
		public function listConnectediOSDevices():void{
			applicationController.consoleController.log("Listing Connected iOS Devices ...", ColorUtils.BLUE);
			
			applicationController.commandHandler.addEventListener(DeploymentEvent.IOS_DEVICES_LISTED, onListiOSDevices);
			applicationController.commandHandler.listConnectediOSDevices();
		}
		
		//Handlers
		public function onListAndroidDevices(event:DeploymentEvent):void{
			applicationController.commandExecutor.removeEventListener(DeploymentEvent.ANDROID_DEVICES_LISTED, onListAndroidDevices);
			
			//Create List
			ApplicationModel.instance.androidDeviceList = new ArrayList(event.data as Array);
			
			if(ApplicationModel.instance.androidDeviceList.length == 0){
				trace("will wait for device");
				applicationController.commandExecutor.executeCommand(CommandConstants.WAIT_FOR_DEVICE);
			}else{
				ApplicationModel.instance.currentDeviceVO = ApplicationModel.instance.androidDeviceList.getItemAt(0) as DeviceVO;
			}
			
			ApplicationModel.instance.devicesLoadedOnce = true;
			
			applicationController.application.localDeployController.update();
			applicationController.application.packageManagerController.update();
			applicationController.application.deviceInfoController.update();
			applicationController.application.remoteDeployController.update();
			applicationController.application.screenRecordController.update();
		}
		
		private function onListiOSDevices(event:DeploymentEvent):void{
			ApplicationModel.instance.iosDeviceList = new ArrayList(event.data);
			
			if(applicationController.application.singleDeployComponent.deviceComboBox)
				applicationController.application.singleDeployComponent.deviceComboBox.dataProvider = ApplicationModel.instance.iosDeviceList;
			
			applicationController.commandExecutor.removeEventListener(DeploymentEvent.IOS_DEVICES_LISTED, onListiOSDevices);
		}
		
		public static function get instance():ApplicationModel{
			return _instance;
		}
	}
}