package data
{
	public class SharedObjectKeys
	{
		public static const LAST_DEPLOYED_BINARY:String = "lastDeployedBinary";
		public static const LAST_DEPLOYED_REMOTE_BINARY:String = "lastDeployedRemoteBinary";
		public static const LAST_GENERATED_BATCH:String = "lastGeneratedBatch";
		public static const ECHO_BATCH:String = "echoBatch";
		public static const REINSTALL_BATCH:String = "reinstallBatch";
		public static const INSTALL_ON_SD_BATCH:String = "installOnSdBatch";
		public static const LAST_VIDEO_FILE:String = "lastVideoFile";
		public static const LAST_VIDEO_TIME_LIMIT:String = "lastVideoTimeLimit";
		public static const LAST_VIDEO_BIT_RATE:String = "lastVideoBitRate";
		public static const LAST_VIDEO_WIDTH:String = "lastVideoWidth";
		public static const LAST_VIDEO_HEIGHT:String = "lastVideoHeight";
		public static const LAST_VIDEO_VERBOSE:String = "lastVideoVerbose";
		public static const LAST_VIDEO_ROTATE:String = "lastVideoRotate";
		public static const LAST_VIDEO_ON_DESKTOP:String = "lastVideoOnDesktop";
		public static const REINSTALL_FLAG:String = "reinstall";
		public static const REMOTE_REINSTALL_FLAG:String = "remoteReinstall";
		public static const DELETE_FILE_FLAG:String = "deleteTempFile";
		
		public static const DETECT_DEVICES_ON_LAUNCH:String = "detectOnLaunch";
		public static const PACKAGE_BACKUP_LOCATION:String = "packageBackupLocation";
	}
}