package Utils
{
	public class ColorUtils
	{
		public static const RED:String = "#FF0000"; //Error
		public static const GREEN:String = "#007236"; //Success message
		public static const BLUE:String = "#0000FF"; //Information message
		public static const YELLOW:String = "#ffc000"; //
		public static const ORANGE:String = "#f7941d";//Command
		public static const GREY:String = "#666666"; //Default
	}
}